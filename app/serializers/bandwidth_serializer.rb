# frozen_string_literal: true

class BandwidthSerializer < ActiveModel::Serializer
  attributes :value_id, :interface_name, :value
end
