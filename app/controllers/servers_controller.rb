# frozen_string_literal: true

class ServersController < ApplicationController
  def index
    @pagy, @servers = pagy(Server.all, items: params[:per_page])
  end
end
