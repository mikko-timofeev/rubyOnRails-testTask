# frozen_string_literal: true

Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      resources :servers, only: [:index, :destroy], param: :id do
        resources :interfaces, only: [:index, :destroy], param: :id, controller: 'servers/bandwidths'
      end
    end
  end

  resources :servers, only: [:index], param: :id do
    resources :interfaces, only: [:index], controller: 'bandwidths'
  end

  root 'servers#index'
end
