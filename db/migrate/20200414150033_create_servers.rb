# frozen_string_literal: true

class CreateServers < ActiveRecord::Migration[6.0]
  def change
    create_table :servers, id: :serial do |t|
      t.text :name, null: false, unique: true

      t.timestamps
    end
  end
end
