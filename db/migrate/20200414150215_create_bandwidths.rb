# frozen_string_literal: true

class CreateBandwidths < ActiveRecord::Migration[6.0]
  def change
    create_table :bandwidths do |t|
      t.belongs_to :server

      t.float :value, null: false
      t.text :interface_name, null: false, index: true
      t.serial :value_id, null: false

      t.timestamps
    end
  end
end
