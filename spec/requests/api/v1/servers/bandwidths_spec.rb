# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'API V1 interfaces at server' do
  describe 'GET /api/v1/servers/:server_id/interfaces' do
    subject :get_interfaces do
      get "/api/v1/servers/#{server.id}/interfaces", headers: { 'Content-Type' => 'application/json' }
    end

    let(:server) { create(:server) }
    let(:interfaces) { create_list(:bandwidth, 3, server: server) }

    before do
      get_interfaces
    end

    it 'returns list of interfaces at this server' do
      expect(response.status).to eq(200)

      # TODO
    end
  end

  describe 'DELETE to /api/v1/servers/:server_id/interfaces/:interface_id' do
    subject :delete_interface do
      delete "/api/v1/servers/#{serv.id}/interfaces/#{interface.id}", headers: { 'Content-Type' => 'application/json' }
    end

    let(:serv) { create(:server) }
    let(:interface) { create(:bandwidth, server: serv) }

    before do
      delete_interface
    end

    it 'removes this interface from this server' do
      expect(response.status).to eq(200)

      # TODO
    end
  end
end
