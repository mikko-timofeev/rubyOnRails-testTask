# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Servers' do
  describe 'GET to /servers' do
    subject(:get_servers) { get '/servers' }

    context 'when server exists' do
      let!(:server) { create(:server, id: 3) }

      it do
        get_servers

        expect(response.status).to eq(200)
        expect(response.body).to include(server.name)
      end
    end
  end
end
